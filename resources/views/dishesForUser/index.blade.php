@extends('layouts.app')

@section('content')

@include('partials.errors', ['errors'=>$errors])

<div class="bg-rez">
      <img src="{{$url = asset('/images/ws_Spring_Forest_1920x1080.jpg')}}">
</div>

<div class="row text-center">
    @foreach($dishes as $dish)
    <div class="col-md-3 col-sm-6 col-xs-12 hero-feature ">
        <div class="thumbnail">
            <img  class="img-responsive" width="500" height="250" alt="dish image" src="{{$dish->getPhotoUrl() }}">
            <div class="caption">
                {!! Form::open(['route' => ['cart.update', $dish->id], 'method' => 'PUT']) !!}
                <h3>{{ $dish->title }}</h3>
                <p>{!!$dish->description !!}</p>
                <P>Price: {!!$dish->getSalePrice()!!} eur</P>
                
<!-- quantity selector -->
                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" id="{{$dish->id}}" class="quantity-left-minus btn btn-danger btn-number"  data-type="minus" data-field="" >
                            <span>-</span>
                        </button>
                    </span>

                    <input type="text" id="{{$dish->id}}" name="quantity" class="form-control input-number text-center" value="1">

                    <span class="input-group-btn">
                        <button type="button" id="{{$dish->id}}" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">
                            <span>+</span>
                        </button>
                    </span>
                </div></br>
<!-- quantity selector -->
              {!! Form::submit('Add to cart!', array('class' => 'btn btn-primary')) !!}
              {!! Form::close() !!}
            </div>
        </div>
    </div>
    @endforeach
</div>


@endsection